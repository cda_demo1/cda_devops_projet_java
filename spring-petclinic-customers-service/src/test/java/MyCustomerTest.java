// Importation des classes nécessaires
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.samples.petclinic.customers.CustomersServiceApplication;

@SpringBootTest(classes = CustomersServiceApplication.class)
@AutoConfigureMockMvc
public class MyCustomerTest {
    // Cette annotation indique que le MockMvc doit être injecté.
    @Autowired
    private MockMvc mockMvc;

    // Cette méthode de test vérifie que la requête GET /owners/find renvoie le code d'état HTTP 200.
    @Test
    public void testFind() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/owners/1")) //Vérifie que le statut de la réponse est ok (200)
            .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
